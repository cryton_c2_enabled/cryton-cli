[[_TOC_]]

![Coverage](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-cli/badges/master/coverage.svg)

# Cryton CLI

## Description
Cryton CLI is a command Line Interface for working with Cryton. It uses REST API, which is part of 
[Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core).

[Link to the documentation](https://beast-public.gitlab-pages.ics.muni.cz/cryton/cryton-documentation/).

## Installation
Important note: this guide only explains how to install **Cryton CLI** package. to be able to execute the 
attack scenarios, you also need to install the **[Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core)** 
and **[Cryton Worker](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-worker)** package.

**Dependencies**
- Python >=3.8
- pipenv (optional)

For manual installation all you need to do is **run the setup script** (it is recommended to use virtual environment, see the next step).
```
python3.8 setup.py install
```

It is recommended to **use virtual environment for installation**, for example using pipenv.
```
pipenv shell
python setup.py install
```

Everything should be set. Check if the installation was successful using:
```
cryton-cli
```

You should see a help page.

If there is an error due to missing *environment variables* go to the [settings section](#settings).

### Development
For development, all you need to do is use `develop` instead of `install`.
```
pipenv shell
python setup.py develop
```

## Settings
Cryton CLI uses environment variables for its settings. Please update variables for your use case.
```
CRYTON_CLI_TIME_ZONE=AUTO
CRYTON_CLI_API_RHOST=127.0.0.1
CRYTON_CLI_API_RPORT=8000
CRYTON_CLI_API_SSL=False
CRYTON_CLI_API_ROOT_URL=cryton/api/v1/
```

If you're using *pipenv* as your Python virtual environment, re-entering it should be enough to load variables from *.env* file.  
To update environment variable you can use *export* command. For example: `export CRYTON_CLI_API_RHOST=localhost`.
Some environment variables can be overridden in CLI. Try using `cryton-cli --help`.

Settings description: 
- `CRYTON_CLI_TIME_ZONE` - (**string**) What timezone (*UTC*, *US/Eastern*, ...) to use for scheduling (for example 
when scheduling a run), by default your system timezone is used
- `CRYTON_CLI_API_RHOST` - (**string**) REST API address used for connection
- `CRYTON_CLI_API_RPORT` - (**int**) REST API port used for connection
- `CRYTON_CLI_API_SSL` - (**boolean - True/False**) Use SSL to connect to REST API
- `CRYTON_CLI_API_ROOT_URL` - (**string**) REST API url with version (do not change, if you don't know what you're doing)

## Usage
In order to make CLI work, you need to install the [Cryton Core](https://gitlab.ics.muni.cz/beast-public/cryton/cryton-core)
app, since CLI is implementing its REST API.

To change the default REST API host/port use *-H* and *-p* options (to permanently change them, see the [settings section](#settings)).
```
cryton-cli -H 127.0.0.1 -p 8000 <your command>
```

To learn about each command's options use:
```
cryton-cli <your command> --help
```

For better understanding of results, we highlight the successful ones with **green** and the others with **red**.

### Example
#### 1. Create Plan template
Create a Plan template using a file containing the desired plan YAML.
```
cryton-cli plan-templates create my-plan.yml 
Template successfully created! ({'plan_template_id': 1, 'link': '...'}).
```

#### 2. Create Plan Instance
Create a Plan instance using saved Plan template.
```
cryton-cli plans create 1
Plan successfully created! ({'plan_model_id': 1, 'link': '...'}).
```

Create a Plan instance using template and inventory file.
```
cryton-cli plans create 1 -i hosts
Plan successfully created! ({'plan_model_id': 1, 'link': '...'}).
```

#### 3. Create Worker
To be able to execute Plans (Runs) we have to define a Worker.
```
cryton-cli workers create <address> <name> -p customPrefix
Worker successfully created! ({'worker_model_id': 1, 'link': '...'}).
```

```
cryton-cli workers create <address> <name> -p customPrefix2
Worker successfully created! ({'worker_model_id': 2, 'link': '...'}).
```

#### 4. Create Run
Create a Run by choosing Plan Instance and providing list of Workers for execution.
```
cryton-cli runs create 1 1 2
Run successfully created! ({'run_model_id': 1, 'link': '...'}).
```

#### 5. Create Execution variables
Create execution Variables for PlanExecution 1.
```
cryton-cli execution-variables create 1 inventory-file
Execution variable(s) successfully created! ([1]).
```

#### 6. Schedule or Execute Run
You can either Schedule the Run for specific date/time, or execute it directly. Run will then be executed on every Worker 
simultaneously.

**Execute a Run**
```
cryton-cli runs execute 1
Run successfully executed! (Run 1 was executed.).
```

**Schedule a Run**
You can schedule a Run using local timezone.
```
cryton-cli runs schedule 1 2020-06-08 10:00:00
Run successfully scheduled! (Run 1 is scheduled for 2020-06-08 10:00:00.).
```

Or you can schedule it using UTC timezone with flag `--utc-timezone`. Otherwise, your preset timezone is used.

#### 7. Read final report
Anytime during the execution a report can be generated, which also complies to YAML format, and it contains list of 
Stages/Steps and their results. Timestamps are by default displayed in UTC timezone, use `--localize` flag to display 
them using your preset timezone.
```
cryton-cli runs report 1
Successfully created Run's report! (file saved at: /tmp/report_run_1_2020-06-08-10-15-00-257994_xdQeV)
```
