from unittest import TestCase
from mock import patch
from click.testing import CliRunner
import requests
import random
import string
import os
import yaml

from cryton_cli.lib.cli import cli


def get_random_name():
    tail = ''.join(random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=7))
    return '/tmp/cryton_test_file{}'.format(tail)


class CliTest(TestCase):
    def setUp(self):
        self.runner = CliRunner()
        self.response = requests.Response()
        self.response.status_code = 200
        self.response._content = b'{"results": {"id": 10}}'

    def test_cli(self):
        result = self.runner.invoke(cli)
        self.assertEqual(0, result.exit_code)

    def test_runs(self):
        result = self.runner.invoke(cli, ['runs'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_runs_list(self):
        with patch('cryton_cli.lib.commands.run.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_runs_create(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'create', '1', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

    def test_runs_read(self):
        with patch('cryton_cli.lib.commands.run.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_runs_delete(self):
        with patch('cryton_cli.lib.commands.run.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_runs_execute(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'execute', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('executed', result.output)

    def test_runs_pause(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'pause', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('paused', result.output)

    def test_runs_postpone(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'postpone', '1', '1', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('postponed', result.output)

    def test_runs_report(self):
        with patch('cryton_cli.lib.commands.run.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'report', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

    def test_runs_report_save_err(self):
        with patch('cryton_cli.lib.commands.run.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'report', '1', '-f', '/tmp/non-existing-dir/my-fake-file12D5sS'])
            self.assertEqual(2, result.exit_code)

    def test_runs_reschedule(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'reschedule', '1', '2020-1-1', '20:20:20'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('rescheduled', result.output)

    def test_runs_reschedule_utc(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'reschedule', '1', '2020-1-1', '20:20:20', '--utc-timezone'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('rescheduled', result.output)

    def test_runs_schedule(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'schedule', '1', '2020-1-1', '20:20:20'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('scheduled', result.output)

    def test_runs_schedule_utc(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'schedule', '1', '2020-1-1', '20:20:20', '--utc-timezone'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('scheduled', result.output)

    def test_runs_unpause(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'resume', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('resumed', result.output)

    def test_runs_unschedule(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'unschedule', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('unscheduled', result.output)

    def test_runs_kill(self):
        with patch('cryton_cli.lib.commands.run.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['runs', 'kill', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('kill', result.output)

    def test_plan_templates(self):
        result = self.runner.invoke(cli, ['plan-templates'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_plan_template_list(self):
        with patch('cryton_cli.lib.commands.plan_template.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-templates', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_template_create(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"plan": "plan"}, f)

        with patch('cryton_cli.lib.commands.plan_template.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-templates', 'create', file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_template_read(self):
        with patch('cryton_cli.lib.commands.plan_template.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-templates', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_template_delete(self):
        with patch('cryton_cli.lib.commands.plan_template.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-templates', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_execution_variables(self):
        result = self.runner.invoke(cli, ['execution-variables'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_execution_variable_list(self):
        with patch('cryton_cli.lib.commands.execution_variable.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['execution-variables', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_execution_variable_create(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"plan": "plan"}, f)

        with patch('cryton_cli.lib.commands.execution_variable.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['execution-variables', 'create', '1', file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_execution_variable_read(self):
        with patch('cryton_cli.lib.commands.execution_variable.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['execution-variables', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_execution_variable_delete(self):
        with patch('cryton_cli.lib.commands.execution_variable.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['execution-variables', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_plans(self):
        result = self.runner.invoke(cli, ['plans'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_plans_list(self):
        with patch('cryton_cli.lib.commands.plan.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plans_create(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'create', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

    def test_plans_create_file_option(self):
        inv_file = get_random_name()
        with open(inv_file, 'w') as f:
            f.write('')

        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'create', '1', '-i', inv_file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

    def test_plans_read(self):
        with patch('cryton_cli.lib.commands.plan.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plans_delete(self):
        with patch('cryton_cli.lib.commands.plan.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_plans_execute(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'execute', '1', '1', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('executed', result.output)

    def test_plans_validate(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"plan": "plan"}, f)

        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plans', 'validate', file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('valid', result.output)
        os.remove(file)

    def test_plan_executions(self):
        result = self.runner.invoke(cli, ['plan-executions'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_plan_executions_list(self):
        with patch('cryton_cli.lib.commands.plan.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_executions_read(self):
        with patch('cryton_cli.lib.commands.plan.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_executions_pause(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'pause', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_executions_report(self):
        with patch('cryton_cli.lib.commands.plan.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'report', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('report', result.output)

    def test_plan_executions_unpause(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'resume', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_executions_validate_modules(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'validate-modules', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_plan_executions_kill(self):
        with patch('cryton_cli.lib.commands.plan.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['plan-executions', 'kill', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('kill', result.output)

    def test_stages(self):
        result = self.runner.invoke(cli, ['stages'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_stages_list(self):
        with patch('cryton_cli.lib.commands.stage.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_stages_create(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"stage": "stage"}, f)

        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'create', file, '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value='str'):
            result = self.runner.invoke(cli, ['stages', 'create', file, '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('wrong', result.output)
        os.remove(file)

    def test_stages_read(self):
        with patch('cryton_cli.lib.commands.stage.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_stages_delete(self):
        with patch('cryton_cli.lib.commands.stage.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_stages_execute(self):
        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'execute', '1', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('executed', result.output)

    def test_stages_validate(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"stage": "stage"}, f)

        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stages', 'validate', file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('valid', result.output)
        os.remove(file)

    def test_stage_executions(self):
        result = self.runner.invoke(cli, ['stage-executions'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_stage_executions_list(self):
        with patch('cryton_cli.lib.commands.stage.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stage-executions', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_stage_executions_read(self):
        with patch('cryton_cli.lib.commands.stage.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stage-executions', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_stage_executions_report(self):
        with patch('cryton_cli.lib.commands.stage.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stage-executions', 'report', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('report', result.output)

    def test_stage_executions_kill(self):
        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stage-executions', 'kill', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('kill', result.output)

    def test_stage_executions_re_execute(self):
        with patch('cryton_cli.lib.commands.stage.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['stage-executions', 're-execute', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('re-execute', result.output)

    def test_steps(self):
        result = self.runner.invoke(cli, ['steps'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_steps_list(self):
        with patch('cryton_cli.lib.commands.step.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_steps_create(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"step": "step"}, f)

        with patch('cryton_cli.lib.commands.step.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'create', file, '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

        with patch('cryton_cli.lib.commands.step.api.post_request', return_value='str'):
            result = self.runner.invoke(cli, ['steps', 'create', file, '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('wrong', result.output)
        os.remove(file)

    def test_steps_read(self):
        with patch('cryton_cli.lib.commands.step.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_steps_delete(self):
        with patch('cryton_cli.lib.commands.step.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_steps_execute(self):
        with patch('cryton_cli.lib.commands.step.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'execute', '1', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('executed', result.output)

    def test_steps_validate(self):
        file = get_random_name()
        with open(file, 'w') as f:
            yaml.dump({"step": "step"}, f)

        with patch('cryton_cli.lib.commands.step.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['steps', 'validate', file])
            self.assertEqual(0, result.exit_code)
            self.assertIn('valid', result.output)
        os.remove(file)

    def test_step_executions(self):
        result = self.runner.invoke(cli, ['step-executions'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_step_executions_list(self):
        with patch('cryton_cli.lib.commands.step.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['step-executions', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_step_executions_read(self):
        with patch('cryton_cli.lib.commands.step.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['step-executions', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_step_executions_report(self):
        with patch('cryton_cli.lib.commands.step.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['step-executions', 'report', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('report', result.output)

    def test_step_executions_kill(self):
        with patch('cryton_cli.lib.commands.step.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['step-executions', 'kill', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('kill', result.output)

    def test_step_executions_re_execute(self):
        with patch('cryton_cli.lib.commands.step.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['step-executions', 're-execute', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('re-execute', result.output)

    def test_workers(self):
        result = self.runner.invoke(cli, ['workers'])
        self.assertEqual(0, result.exit_code)
        self.assertIn('help', result.output)

    def test_workers_list(self):
        with patch('cryton_cli.lib.commands.worker.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['workers', 'list'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_workers_create(self):
        with patch('cryton_cli.lib.commands.worker.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['workers', 'create', 'address', 'name'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('created', result.output)

    def test_workers_read(self):
        with patch('cryton_cli.lib.commands.worker.api.get_request', return_value=self.response):
            result = self.runner.invoke(cli, ['workers', 'show', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('id', result.output)

    def test_workers_delete(self):
        with patch('cryton_cli.lib.commands.worker.api.delete_request', return_value=self.response):
            result = self.runner.invoke(cli, ['workers', 'delete', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('deleted', result.output)

    def test_workers_health_check(self):
        with patch('cryton_cli.lib.commands.worker.api.post_request', return_value=self.response):
            result = self.runner.invoke(cli, ['workers', 'health-check', '1'])
            self.assertEqual(0, result.exit_code)
            self.assertIn('checked', result.output)
