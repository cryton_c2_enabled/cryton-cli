from unittest import TestCase
from mock import patch, MagicMock
import requests_mock
import requests

from cryton_cli.lib.util import api


class ApiTest(TestCase):
    def setUp(self):
        self.response_text = '{"detail": "response"}'

    def test_create_rest_api_url(self):
        ret = api.create_rest_api_url('test', 8000, False)
        self.assertEqual('http://test:8000/', ret)

    def test_create_rest_api_url_ssl(self):
        ret = api.create_rest_api_url('test', 8000, True)
        self.assertEqual('https://test:8000/', ret)

    @requests_mock.mock()
    def test_request_post(self, mock_requests):
        mock_requests.post('http://test.test/test/', text=self.response_text)
        ret = api.post_request('http://test.test/', 'test/', 1)
        self.assertEqual(self.response_text, ret.text)

    @requests_mock.mock()
    def test_request_get(self, mock_requests):
        mock_requests.get('http://test.test/test/', text=self.response_text)
        ret = api.get_request('http://test.test/', 'test/', 1)
        self.assertEqual(self.response_text, ret.text)

    @requests_mock.mock()
    def test_request_delete(self, mock_requests):
        mock_requests.delete('http://test.test/test/', text=self.response_text)
        ret = api.delete_request('http://test.test/', 'test/', 1)
        self.assertEqual(self.response_text, ret.text)

    @patch('cryton_cli.lib.util.api.requests.post')
    def test_request_post_conn_err(self, mock_request):
        mock_request.side_effect = MagicMock(side_effect=requests.exceptions.ConnectionError)
        ret = api.post_request('http://test.test/', 'test/', 1)
        self.assertIn('http://test.test/', ret)

    @patch('cryton_cli.lib.util.api.requests.get')
    def test_request_get_conn_err(self, mock_request):
        mock_request.side_effect = MagicMock(side_effect=requests.exceptions.ConnectionError)
        ret = api.get_request('http://test.test/', 'test/', 1)
        self.assertIn('http://test.test/', ret)

    @patch('cryton_cli.lib.util.api.requests.delete')
    def test_request_delete_conn_err(self, mock_request):
        mock_request.side_effect = MagicMock(side_effect=requests.exceptions.ConnectionError)
        ret = api.delete_request('http://test.test/', 'test/', 1)
        self.assertIn('http://test.test/', ret)
