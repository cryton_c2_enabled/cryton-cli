from unittest import TestCase
from mock import patch, MagicMock
import requests_mock
import requests
import click
import os
import yaml
import json
import string
import random
from datetime import datetime

from cryton_cli.lib.util import util


def get_random_name():
    tail = ''.join(random.choices(string.ascii_uppercase + string.digits + string.ascii_lowercase, k=7))
    return '/tmp/cryton_test_file{}'.format(tail)


class UtilTest(TestCase):
    def setUp(self):
        self.plan_dict = {"plan": "plan"}

    def test_read_config(self):
        config = {"test": "configuration"}
        config_ini = ['[SECTION]\n', 'test: configuration']
        config_wrong = [': :']

        json_inventory = get_random_name()
        with open(json_inventory, "w") as config_file:
            json.dump(config, config_file)
        ret = util.read_config(json_inventory)
        self.assertEqual(ret, config)

        yaml_inventory = get_random_name()
        with open(yaml_inventory, "w") as config_file:
            yaml.dump(config, config_file)
        ret = util.read_config(yaml_inventory)
        self.assertEqual(ret, config)

        ini_inventory = get_random_name()
        with open(ini_inventory, "w") as config_file:
            config_file.writelines(config_ini)
        ret = util.read_config(ini_inventory)
        self.assertEqual(ret.get('SECTION'), config)

        wrong_inventory = get_random_name()
        with open(wrong_inventory, "w") as config_file:
            config_file.writelines(config_wrong)
        with self.assertRaises(ValueError):
            util.read_config(wrong_inventory)

    @patch('cryton_cli.lib.util.util.jinja2.Environment', MagicMock)
    @patch('cryton_cli.lib.util.util.read_config', MagicMock)
    def test_fill_template(self):
        template_file = get_random_name()
        inventory_file = get_random_name()

        with open(template_file, 'w') as f:
            yaml.dump(self.plan_dict, f)
        with open(inventory_file, 'w'):
            pass

        ret = util.fill_template(template_file, (inventory_file,))
        os.remove(template_file)
        os.remove(inventory_file)
        self.assertIsNotNone(ret)

    def test_get_yaml_from_file(self):
        plan_file = get_random_name()

        with open(plan_file, 'w') as f:
            yaml.dump(self.plan_dict, f)

        ret = util.get_yaml_from_file(plan_file)
        os.remove(plan_file)
        self.assertEqual(self.plan_dict, ret)

    @patch('cryton_cli.lib.util.util.fill_template')
    def test_get_yaml_from_file_template(self, mock_fill):
        mock_fill.return_value = yaml.dump(self.plan_dict)
        template_file = get_random_name()
        inventory_file = get_random_name()

        with open(template_file, 'w') as f:
            yaml.dump(self.plan_dict, f)
        with open(inventory_file, 'w'):
            pass

        ret = util.get_yaml_from_file(template_file, (inventory_file,))
        os.remove(template_file)
        os.remove(inventory_file)
        self.assertEqual(self.plan_dict, ret)

    def test_save_report_to_file(self):
        ret = util.save_report_to_file(self.plan_dict, '/tmp', 'test')

        os.remove(ret[1])
        self.assertEqual(0, ret[0])
        self.assertIn('/tmp/report_test', ret[1])

    def test_save_report_to_file_err(self):
        ret = util.save_report_to_file(self.plan_dict, '/tmp/123non-ExiSteNT_FolDER321/285non-ExiSteNT_FilE582')

        self.assertEqual(-1, ret[0])

    @patch('cryton_cli.lib.util.util.config.TIME_ZONE', 'utc')
    def test_convert_from_utc(self):
        test_datetime = datetime.utcnow()
        str_test_datetime = str(test_datetime).replace(' ', 'T') + "Z"
        ret = util.convert_from_utc(str_test_datetime)
        self.assertEqual(test_datetime, ret)


class CliUtilTest(TestCase):
    def setUp(self):
        self.request_url = 'http://test.test/test/'
        self.response_text = '{"detail": "response"}'
        self.response_json = {'detail': 'response'}

    @patch('cryton_cli.lib.util.api.create_rest_api_url')
    def test_CliContext(self, mock_url):
        mock_url.return_value = 'test'
        ret = util.CliContext(None, None, False, False)

        self.assertEqual(mock_url.return_value, ret.api_url)

    @requests_mock.mock()
    def test_get_response_data(self, mock_requests):
        mock_requests.get(self.request_url, text=self.response_text)
        response = requests.get(self.request_url)
        ret = util.get_response_data(response)
        self.assertEqual(self.response_json, ret)

    @requests_mock.mock()
    def test_get_response_data_error_ok(self, mock_requests):
        mock_requests.get(self.request_url, text='text', status_code=200)
        response = requests.get(self.request_url)
        ret = util.get_response_data(response)
        self.assertEqual('Couldn\'t parse response details.', ret)

    @requests_mock.mock()
    def test_get_response_data_error_not(self, mock_requests):
        mock_requests.get(self.request_url, text='text', status_code=500)
        response = requests.get(self.request_url)
        ret = util.get_response_data(response)
        self.assertEqual('Couldn\'t parse response details. Object with ID you specified probably doesn\'t exist.', ret)

    def test_get_detailed_message_str(self):
        data = 'text'
        ret = util.get_detailed_message(data)
        self.assertEqual('text', ret)

    def test_get_detailed_message_dict_results(self):
        data = {'results': 'text'}
        ret = util.get_detailed_message(data)
        self.assertEqual('text', ret)

    def test_get_detailed_message_dict_details(self):
        data = {'details': 'text'}
        ret = util.get_detailed_message(data)
        self.assertEqual('text', ret)

    def test_get_detailed_message_dict_detail(self):
        data = {'detail': 'text'}
        ret = util.get_detailed_message(data)
        self.assertEqual('text', ret)

    def test_get_detailed_message_dict_other(self):
        data = {'other': 'text'}
        ret = util.get_detailed_message(data)
        self.assertEqual({'other': 'text'}, ret)

    @requests_mock.mock()
    def test_echo_msg(self, mock_requests):
        mock_requests.get(self.request_url, text=self.response_text)
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_msg(response)
        click_echo.echo.assert_called_with('\x1b[32mSuccess!\x1b[0m (response).')

    @requests_mock.mock()
    def test_echo_msg_debug(self, mock_requests):
        mock_requests.get(self.request_url, text=self.response_text)
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_msg(response, debug=True)
        click_echo.echo.assert_called_with('\x1b[32mSuccess!\x1b[0m ({"detail": "response"}).')

    @requests_mock.mock()
    def test_echo_msg_fail(self, mock_requests):
        mock_requests.get(self.request_url, text=self.response_text, status_code=500)
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_msg(response)
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (None: response).')

    @staticmethod
    def test_echo_msg_conn_err():
        response = 'err'
        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_msg(response)
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (err).')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = [{'id': 10, 'name': 'name', 'ignore': 'ignore me'}]
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name'])
        click_echo.echo.assert_called_with('id: 10, name: name')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_empty(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = []
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['ignore'])
        click_echo.echo.assert_called_with('\x1b[32mEmpty response...\x1b[0m')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_debug(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = [{'id': 10, 'name': 'name', 'ignore': 'ignore me'}]
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name'], debug=True)
        click_echo.echo.assert_called_with('{"detail": "response"}')

    @patch('cryton_cli.lib.util.util.config.TIME_ZONE', 'utc')
    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_localize(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = [{'id': 10, 'name': 'name', 'ignore': 'ignore me', 'pause_time': '2020-1-1T1:1:1.1Z'}]
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name', 'pause_time'], localize=True)
        click_echo.echo.assert_called_with('id: 10, name: name, pause_time: 2020-01-01 01:01:01.100000')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_more(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = [{'id': 10, 'name': 'name', 'ignore': 'ignore me'},
                                 {'id': 11, 'name': 'name', 'ignore': 'ignore me'}]
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name'])
        click_echo.echo.assert_called_with('id: 11, name: name')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_one(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = {'id': 10, 'name': 'name', 'ignore': 'ignore me'}
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name'])
        click_echo.echo.assert_called_with('id: 10, name: name')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_pager(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = [{'id': 10, 'ignore': 'ignore me'}]
        response = requests.get(self.request_url)
        click_echo = click
        click_echo.echo_via_pager = MagicMock(return_value=1)

        util.echo_list(response, ['id', 'name'], True)
        click_echo.echo_via_pager.assert_called_with(['id: 10, name: None'])

    @staticmethod
    def test_echo_list_conn_err():
        response = 'err'
        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['ignore'])
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (err).')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_echo_list_one_err(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text, status_code=500, reason='test')
        mock_msg.return_value = 'err'
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.echo_list(response, ['ignore'])
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (test: err).')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = {"detail": {"report": {}}}
        mock_report.return_value = (0, 'file-location')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '/tmp', 'test')
        click_echo.echo.assert_called_with('\x1b[32mSuccessfully created report!\x1b[0m (file saved at: file-location)')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report_echo_only(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = {"detail": {"report": {}}}
        mock_report.return_value = (0, 'file-location')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '', '', True)
        click_echo.echo.assert_called_once()

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    @patch('cryton_cli.lib.util.util.update_report', MagicMock())
    def test_get_report_echo_only_less(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = {"detail": {"report": {}}}
        mock_report.return_value = (0, 'file-location')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo_via_pager = MagicMock(return_value=1)

        util.get_report(response, '', '', True, True, False, True)
        click_echo.echo_via_pager.assert_called_once()

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report_echo_only_less_debug(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = 'ok'
        mock_report.return_value = (0, 'file-location')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo_via_pager = MagicMock(return_value=1)

        util.get_report(response, '', '', True, True, True)
        click_echo.echo_via_pager.assert_called_once()

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report_echo_only_debug(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = 'ok'
        mock_report.return_value = (0, 'file-location')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '', '', True, debug=True)
        click_echo.echo.assert_called_once()

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.save_report_to_file')
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report_save_err(self, mock_requests, mock_msg, mock_report):
        mock_requests.get(self.request_url, text=self.response_text)
        mock_msg.return_value = {"detail": {"report": {}}}
        mock_report.return_value = (-1, 'err')
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '/tmp', 'test')
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (details: err)')

    @requests_mock.mock()
    @patch('cryton_cli.lib.util.util.get_detailed_message')
    @patch('cryton_cli.lib.util.util.get_response_data', MagicMock())
    def test_get_report_req_err(self, mock_requests, mock_msg):
        mock_requests.get(self.request_url, text=self.response_text, status_code=500, reason='test')
        mock_msg.return_value = 'err'
        response = requests.get(self.request_url)

        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '/tmp', 'test')
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (test: err).')

    @staticmethod
    def test_get_report_conn_err():
        response = 'err'
        click_echo = click
        click_echo.echo = MagicMock(return_value=1)

        util.get_report(response, '/tmp', 'test')
        click_echo.echo.assert_called_with('\x1b[31mSomething went wrong :/\x1b[0m (err).')

    @patch('cryton_cli.lib.util.util.convert_from_utc')
    def test_update_report(self, mock_utc):
        mock_utc.return_value = 'test'
        custom_report = {'var1': {'list': [{'pause_time': '2020-1-1T1:1:1.1'}]}}
        util.update_report(custom_report, True)
        self.assertEqual(custom_report, {'var1': {'list': [{'pause_time': 'test'}]}})
