import os
from tzlocal import get_localzone_name

TIME_ZONE = os.getenv('CRYTON_CLI_TIME_ZONE')
if TIME_ZONE is None or TIME_ZONE.lower() == 'auto':
    TIME_ZONE = get_localzone_name()

API_RHOST = os.getenv('CRYTON_CLI_API_RHOST')
API_RPORT = os.getenv('CRYTON_CLI_API_RPORT')
API_SSL = os.getenv('CRYTON_CLI_API_SSL')
API_ROOT_URL = os.getenv('CRYTON_CLI_API_ROOT_URL')
