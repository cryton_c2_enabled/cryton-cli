import requests
from typing import Union

from cryton_cli.etc import config


# Endpoint urls
# Runs
RUN_LIST = config.API_ROOT_URL + 'runs/'
RUN_CREATE = config.API_ROOT_URL + 'runs/'
RUN_READ = config.API_ROOT_URL + 'runs/{}/'
RUN_DELETE = config.API_ROOT_URL + 'runs/{}/'
RUN_EXECUTE = config.API_ROOT_URL + 'runs/{}/execute/'
RUN_PAUSE = config.API_ROOT_URL + 'runs/{}/pause/'
RUN_POSTPONE = config.API_ROOT_URL + 'runs/{}/postpone/'
RUN_REPORT = config.API_ROOT_URL + 'runs/{}/report/'
RUN_RESCHEDULE = config.API_ROOT_URL + 'runs/{}/reschedule/'
RUN_SCHEDULE = config.API_ROOT_URL + 'runs/{}/schedule/'
RUN_UNPAUSE = config.API_ROOT_URL + 'runs/{}/unpause/'
RUN_UNSCHEDULE = config.API_ROOT_URL + 'runs/{}/unschedule/'
RUN_KILL = config.API_ROOT_URL + 'runs/{}/kill/'

# Plans
PLAN_LIST = config.API_ROOT_URL + 'plans/'
PLAN_CREATE = config.API_ROOT_URL + 'plans/'
PLAN_VALIDATE = config.API_ROOT_URL + 'plans/validate/'
PLAN_READ = config.API_ROOT_URL + 'plans/{}/'
PLAN_DELETE = config.API_ROOT_URL + 'plans/{}/'
PLAN_EXECUTE = config.API_ROOT_URL + 'plans/{}/execute/'

# PlanExecutions
PLAN_EXECUTION_LIST = config.API_ROOT_URL + 'plan_executions/'
PLAN_EXECUTION_CREATE = config.API_ROOT_URL + 'plan_executions/'
PLAN_EXECUTION_READ = config.API_ROOT_URL + 'plan_executions/{}/'
PLAN_EXECUTION_PAUSE = config.API_ROOT_URL + 'plan_executions/{}/pause/'
PLAN_EXECUTION_REPORT = config.API_ROOT_URL + 'plan_executions/{}/report/'
PLAN_EXECUTION_UNPAUSE = config.API_ROOT_URL + 'plan_executions/{}/unpause/'
PLAN_EXECUTION_VALIDATE_MODULES = config.API_ROOT_URL + 'plan_executions/{}/validate_modules/'
PLAN_EXECUTION_KILL = config.API_ROOT_URL + 'plan_executions/{}/kill/'

# Stages
STAGE_LIST = config.API_ROOT_URL + 'stages/'
STAGE_CREATE = config.API_ROOT_URL + 'stages/'
STAGE_VALIDATE = config.API_ROOT_URL + 'stages/validate/'
STAGE_READ = config.API_ROOT_URL + 'stages/{}/'
STAGE_DELETE = config.API_ROOT_URL + 'stages/{}/'
STAGE_EXECUTE = config.API_ROOT_URL + 'stages/{}/execute/'

# StageExecutions
STAGE_EXECUTION_LIST = config.API_ROOT_URL + 'stage_executions/'
STAGE_EXECUTION_CREATE = config.API_ROOT_URL + 'stage_executions/'
STAGE_EXECUTION_READ = config.API_ROOT_URL + 'stage_executions/{}/'
STAGE_EXECUTION_REPORT = config.API_ROOT_URL + 'stage_executions/{}/report/'
STAGE_EXECUTION_KILL = config.API_ROOT_URL + 'stage_executions/{}/kill/'
STAGE_EXECUTION_RE_EXECUTE = config.API_ROOT_URL + 'stage_executions/{}/re_execute/'

# Steps
STEP_LIST = config.API_ROOT_URL + 'steps/'
STEP_CREATE = config.API_ROOT_URL + 'steps/'
STEP_VALIDATE = config.API_ROOT_URL + 'steps/validate/'
STEP_READ = config.API_ROOT_URL + 'steps/{}/'
STEP_DELETE = config.API_ROOT_URL + 'steps/{}/'
STEP_EXECUTE = config.API_ROOT_URL + 'steps/{}/execute/'

# StepExecutions
STEP_EXECUTION_LIST = config.API_ROOT_URL + 'step_executions/'
STEP_EXECUTION_CREATE = config.API_ROOT_URL + 'step_executions/'
STEP_EXECUTION_READ = config.API_ROOT_URL + 'step_executions/{}/'
STEP_EXECUTION_REPORT = config.API_ROOT_URL + 'step_executions/{}/report/'
STEP_EXECUTION_KILL = config.API_ROOT_URL + 'step_executions/{}/kill/'
STEP_EXECUTION_RE_EXECUTE = config.API_ROOT_URL + 'step_executions/{}/re_execute/'

# Workers
WORKER_LIST = config.API_ROOT_URL + 'workers/'
WORKER_CREATE = config.API_ROOT_URL + 'workers/'
WORKER_READ = config.API_ROOT_URL + 'workers/{}/'
WORKER_DELETE = config.API_ROOT_URL + 'workers/{}/'
WORKER_HEALTH_CHECK = config.API_ROOT_URL + 'workers/{}/healthcheck/'

# Templates
TEMPLATE_LIST = config.API_ROOT_URL + 'templates/'
TEMPLATE_CREATE = config.API_ROOT_URL + 'templates/'
TEMPLATE_READ = config.API_ROOT_URL + 'templates/{}/'
TEMPLATE_DELETE = config.API_ROOT_URL + 'templates/{}/'

# Execution variables
EXECUTION_VARIABLE_LIST = config.API_ROOT_URL + 'execution_variables/'
EXECUTION_VARIABLE_CREATE = config.API_ROOT_URL + 'execution_variables/'
EXECUTION_VARIABLE_READ = config.API_ROOT_URL + 'execution_variables/{}/'
EXECUTION_VARIABLE_DELETE = config.API_ROOT_URL + 'execution_variables/{}/'


def create_rest_api_url(host: str, port: int, ssl: bool) -> str:
    """
    Create REST API URL
    :param host: Address of the host
    :param port: Port of the host
    :param ssl: If true, use HTTPS, else use HTTP
    :return: REST API URL
    """
    if ssl:
        url_prefix = 'https'
    else:
        url_prefix = 'http'
    return '{}://{}:{}/'.format(url_prefix, host, port)


def get_request(api_url: str, endpoint_url: str, object_id: int = None, custom_params: dict = None) \
        -> Union[str, requests.Response]:
    """
    Custom get request.
    :param api_url: API url
    :param endpoint_url: API endpoint url
    :param object_id: ID of the desired object
    :param custom_params: Optional dictionary containing custom params
    :return: API response
    """
    if object_id is not None:
        endpoint_url = endpoint_url.format(object_id)
    url = api_url + endpoint_url
    try:
        response = requests.get(url, custom_params)
    except requests.exceptions.ConnectionError:
        return 'Cannot connect to {}.'.format(url)

    return response


def post_request(api_url: str, endpoint_url: str, object_id: int = None, custom_dict: dict = None, file: dict = None) \
        -> Union[str, requests.Response]:
    """
    Custom post request.
    :param api_url: API url
    :param endpoint_url: API endpoint url
    :param object_id: ID of the desired object
    :param custom_dict: instance yaml
    :param file: file to be sent
    :return: API response
    """
    if object_id is not None:
        endpoint_url = endpoint_url.format(object_id)
    url = api_url + endpoint_url

    try:
        response = requests.post(url, json=custom_dict, files=file)
    except requests.exceptions.ConnectionError:
        return 'Cannot connect to {}.'.format(url)

    return response


def delete_request(api_url: str, endpoint_url: str, object_id: int = None) -> Union[str, requests.Response]:
    """
    Custom delete request.
    :param api_url: API url
    :param endpoint_url: API endpoint url
    :param object_id: ID of the desired object
    :return: API response
    """
    if object_id is not None:
        endpoint_url = endpoint_url.format(object_id)
    url = api_url + endpoint_url
    try:
        response = requests.delete(url)
    except requests.exceptions.ConnectionError:
        return 'Cannot connect to {}.'.format(url)

    return response
