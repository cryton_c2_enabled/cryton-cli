import click

from cryton_cli.lib.util import util
from cryton_cli.lib.commands import plan_template, run, plan, step, worker, execution_variable, stage


# TODO: cli should pass context with rest api url, etc.. also it will remember latest used plan_id/run_id, etc. to
#  ensure support for command chaining. Example: crycli plan create /path/to/file.yaml (returns id 1) run execute 1
#  (executes plan with id 1, because it's the latest plan created in this CLI)
#  https://click.palletsprojects.com/en/7.x/complex/

# TODO: autocomplete for options and arguments.
#  https://click.palletsprojects.com/en/7.x/api/#click.Parameter for files, etc.
#  https://stackoverflow.com/questions/52282173/extending-bash-completion-with-python-click
#  https://github.com/click-contrib/click-completion/blob/master/examples/click-completion-command
#  https://stackoverflow.com/questions/58577801/python-click-autocomplete-for-str-str-option

# TODO: add ability to show children for list commands with this functionality (also for shell)
@click.group()
@click.option('-H', '--host', type=click.STRING, help='Set Cryton\'s address (default is localhost).')
@click.option('-p', '--port', type=click.INT, help='Set Cryton\'s address (default is 8000).')
@click.option('--secure', is_flag=True, help='Set if HTTPS will be used.')
@click.option('--debug', is_flag=True, help='Show non formatted output.')
@click.version_option(message=f"%(prog)s, version %(version)s")
@click.pass_context
def cli(ctx: click.core.Context, host: str, port: int, secure: bool, debug: bool) -> None:
    """
    A CLI wrapper for Cryton API.
    \f
    :param ctx: Click context
    :param secure: True if use HTTPS for requests, else HTTP (default is False)
    :param debug: Show non formatted output
    :param host: Cryton's REST API url (default is None)
    :param port: Cryton's REST API port (default is None)
    :return: None
    """
    ctx.obj = util.CliContext(host, port, secure, debug)


cli.add_command(run.run)
cli.add_command(plan.plan)
cli.add_command(plan.plan_execution)
cli.add_command(stage.stage)
cli.add_command(stage.stage_execution)
cli.add_command(step.step)
cli.add_command(step.step_execution)
cli.add_command(worker.worker)
cli.add_command(plan_template.template)
cli.add_command(execution_variable.execution_variable)
