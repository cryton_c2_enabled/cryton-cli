import click

from cryton_cli.lib.util import api, util


# Execution variables
@click.group('execution-variables')
@click.pass_obj
def execution_variable(_) -> None:
    """
    Manage Execution variables from here.

    \f
    :param _: Click ctx object
    :return: None
    """


@execution_variable.command('list')
@click.option('--less', is_flag=True, help='Show less like output.')
@click.option('-o', '--offset', type=click.INT, default=0, help='The initial index from which to return the results.')
@click.option('-l', '--limit', type=click.INT, default=0, help='Number of results to return per page.')
@click.option('--localize', is_flag=True, help='Convert UTC datetime to local timezone.')
@click.pass_obj
def execution_variable_list(ctx: util.CliContext, less: bool, offset: int, limit: int, localize: bool) -> None:
    """
    List existing Execution variables in Cryton.

    \f
    :param ctx: Click ctx object
    :param less: Show less like output
    :param offset: Initial index from which to return the results
    :param limit: Number of results per page
    :param localize: If datetime variables should be converted to local timezone
    :return: None
    """
    appendix = '?limit={}&offset={}'.format(limit, offset)
    response = api.get_request(ctx.api_url, api.EXECUTION_VARIABLE_LIST + appendix)

    to_print = ['id', 'name', 'value', 'plan_execution']

    util.echo_list(response, to_print, less, localize, ctx.debug)


@execution_variable.command('create')
@click.argument('plan_execution_id', type=click.INT, required=True)
@click.argument('file', type=click.Path(exists=True), required=True)
@click.pass_obj
def execution_variable_create(ctx: util.CliContext, plan_execution_id: int, file: str) -> None:
    """
    Create new execution variable(s) for PlAN_EXECUTION_ID from FILE.

    PLAN_EXECUTION_ID IS ID of the desired PlanExecution.
    FILE is path to file containing execution variables.

    \f
    :param ctx: Click ctx object
    :param plan_execution_id: ID of the desired PlanExecution.
    :param file: Path to file containing execution variables.
    :return: None
    """
    with open(file, 'r') as f:
        inventory_file = f.read()

    arguments = {'plan_execution_id': plan_execution_id, 'inventory_file': inventory_file}
    response = api.post_request(ctx.api_url, api.EXECUTION_VARIABLE_CREATE, custom_dict=arguments)

    util.echo_msg(response, 'Execution variable(s) successfully created!', ctx.debug)


@execution_variable.command('show')
@click.argument('execution_variable_id', type=click.INT, required=True)
@click.option('--less', is_flag=True, help='Show less like output.')
@click.option('--localize', is_flag=True, help='Convert UTC datetime to local timezone.')
@click.pass_obj
def execution_variable_read(ctx: util.CliContext, execution_variable_id: int, less: bool, localize: bool) -> None:
    """
    Show Execution variable with EXECUTION_VARIABLE_ID saved in Cryton.

    EXECUTION_VARIABLE_ID is ID of the Execution variable you want to see.

    \f
    :param ctx: Click ctx object
    :param execution_variable_id: ID of the desired execution variable
    :param less: Show less like output
    :param localize: If datetime variables should be converted to local timezone
    :return: None
    """
    response = api.get_request(ctx.api_url, api.EXECUTION_VARIABLE_READ, execution_variable_id)

    to_print = ['id', 'name', 'value', 'plan_execution']

    util.echo_list(response, to_print, less, localize, ctx.debug)


@execution_variable.command('delete')
@click.argument('execution_variable_id', type=click.INT, required=True)
@click.pass_obj
def execution_variable_delete(ctx: util.CliContext, execution_variable_id: int) -> None:
    """
    Delete Execution variable with EXECUTION_VARIABLE_ID saved in Cryton.

    EXECUTION_VARIABLE_ID is ID of the Execution_variable you want to delete.

    \f
    :param ctx: Click ctx object
    :param execution_variable_id: ID of the desired execution variable
    :return: None
    """
    response = api.delete_request(ctx.api_url, api.EXECUTION_VARIABLE_DELETE, execution_variable_id)

    util.echo_msg(response, 'Execution variable successfully deleted!', ctx.debug)
